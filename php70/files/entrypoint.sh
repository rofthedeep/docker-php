#!/bin/bash

#echo -n "phpmyadmin settings"
#if [ -f /etc/apache2/phpmyadmin.htpasswd ]
#then
#  HTPASSWD_OPTS=-Bbi
#else
#  HTPASSWD_OPTS=-cBbi
#fi
#
#if [ -n "$PHPMYADMIN_PW" ]; then
#    htpasswd -Bbc /etc/apache2/phpmyadmin.htpasswd phpmyadmin "${PHPMYADMIN_PW}"
#fi

cat > /etc/phpmyadmin/config-db.php << EOF
<?php
\$dbuser='${DB_USER:-slim}';
\$dbpass='${DB_PASSWORD:-slim}';
\$basepath='';
\$dbname='${DB_DATABASE:-slim}';
\$dbserver='${DB_HOST:-${DB_PORT_3306_TCP_ADDR}}';
\$dbport='${DB_PORT:-${DB_PORT_3306_TCP_PORT:-3306}}';
\$dbtype='mysql';
EOF


echo -n "apache foreground"
source /etc/apache2/envvars
exec apache2 -D FOREGROUND