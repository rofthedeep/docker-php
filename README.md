# Docker für Shopware

## Log

### 09.07.2018

- added working images for php71 and php72

### 03.03.2018

- added SSL Offloading Header: used in Magento and other systems. So in Config or PHP SSL Connection must not be set.

### 06.01.2018

- added mysqlLite

### 30.03.2017

- added support for SSL and HTTPS
- CERTS has to be accessible by php and proxy container

## Generierung Docker Images

### Build Image

On Command Line:

    docker build -t docker-image-name .
    -------
    sudo docker build -t rotd/docker-base-php70-proxy-cron-ssl:latest .
    sudo docker build -t rotd/docker-base-php71-proxy-cron-ssl:latest .
    sudo docker build -t rotd/docker-base-php72-proxy-cron-ssl:latest .

### Tag Image

Image ID (7d9495d03763) kann über `docker image` ermittelt werden.

    docker tag 7d9495d03763 rotd/docker-base-php72-proxy-cron-ssl:latest

### Image Upload

    docker login

    docker push rotd/docker-base-php72-proxy-cron-ssl:latest


## Test Image


Run a container for testing purposes:

    docker run -i -t ubuntu:12.04 /bin/bash
    sudo docker exec -i -t 450a066dc7f4 /bin/bash


## Kommandos

- list running containers `docker ps`
- list all containers `docker ps -a`
- list images `docker images`
- kill container `docker kill containerid`
- login into container `docker exec -it containerid bash`
- get ip of container `docker inspect containerid`
- force restart `docker-compose up -d --force-recreate shopware`

[Quelle](https://docs.docker.com/engine/getstarted/step_four/)

## Debugging im Container

### PHP Pfad herausfinden

    php -i | grep extension_dir


### PHP Versiom

    php -v

### Datenbankoperationen

    show databases;
    SELECT User FROM mysql.user;

### Zugangsdaten Mysql

Diese finden sich in folgender Datei im shopware container:

    /etc/phpmyadmin/config-db.php

## phpMyAdmin

Um die Datenbank mit phpMyAdmin zu verwalten, muss die Umgebungsvariable `PHPMYADMIN_PW` gesetzt sein.
Wenn sie nicht gesetzt ist, besteht keine Möglichkeit, sich an phpMyAdmin erfolgreich anzumelden.
phpMyAdmin ist unter der URL [http://localhost/phpmyadmin/](http://localhost/phpmyadmin/) erreichbar.
Sollten Sie Docker nicht auf Ihrem Rechner betreiben, sondern auf einem anderen Server, so ersetzen Sie `localhost` durch den Namen oder die IP des Servers.

Bei dem Zugriff auf phpmyadmin werden Sie zuerst von Ihrem Browser nach Zugangsdaten gefragt.
Der Benutzername ist `phpmyadmin` und das Passwort ist das aus der Umgebungsvariable `PHPMYADMIN_PW`.
Danach werden Sie nach den Zugangsdaten zur `shopware` Datenbank gefragt.
Geben Sie für beides `shopware` ein, es sei denn, Sie haben etwas anderes mit `DB_PASSWORD` gesetzt.

User: phpmyadmin
Passwort: shopware


